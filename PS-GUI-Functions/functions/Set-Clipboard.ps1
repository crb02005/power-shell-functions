
<#
    PowerShell Set- Function
    Copyright (C) 2016  Carl Burks

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#>

function Set-Clipboard(){
Param($data)
<#

.SYNOPSIS

Sets the clipboard with the operand.

.EXAMPLE

Set the clipboard with text

Set-Clipboard "some text"


.EXAMPLE

Set the clipboard from pipeline

echo "some text" | Set-Clipboard

#>
    if(($data -ne $null) -and ($data -ne '')){
        $str = $data
    } else {
        $str = $input | Out-String
    }
    [Windows.Forms.Clipboard]::Clear();
    if  (($str -ne $null) -and ($str -ne '')){
        $str | clip.exe
    }

}

