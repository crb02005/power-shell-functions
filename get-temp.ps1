﻿# Get CPU Temp
(([System.Management.ManagementObjectSearcher]::new("root\WMI","Select * From MSAcpi_ThermalZoneTemperature").Get() | Select-Object -Property CurrentTemperature)  | %{$_.CurrentTemperature -2732 )/10}
