
<#
    PowerShell Calendar Display Function
    Copyright (C) 2016  Carl Burks

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#>

function Show-Calendar($date){
<#

.SYNOPSIS

Displays a Calendar.

.EXAMPLE

Display Calendar with the current date.

Show-Calendar 


.EXAMPLE

Calendar for specific date

Show-Calendar (get-date -date "12/1/2030")

.EXAMPLE
Display a yearly calendar

$startDate = Get-Date 
0..11 | %{
    Show-Calendar  $startDate.AddMonths($_)
}

#>

if(!$date){
    $date = Get-Date
}
$header = get-date $date -Format y
$pad = 21 - $header.Length
$header = (" " * ($pad /2)) + $header + (" " * ($pad /2))
$result += $header

$indexDay = ($date).AddDays(-(get-date $date -Format %d) +1)
$lastDay = $indexDay.AddMonths(1).AddDays(-1)
$daycode = (get-date $indexDay -Format ddd)
$lastDayNumber = (get-date $lastDay -Format %d)
$days = @("Sun","Mon","Tue","Wed","Thu","Fri","Sat")

$result += "`r`n"
$result += " S  M  T  W  T  F  S`r`n"

$start = $false
$stop = $false
$days * 7 | %{
    if(!$stop -and ( $daycode -eq $_ -or $start)){
       $result += (get-date $indexDay -Format dd)
       $start = $true
    } else {
       $result += "  "
    }
    $result += " "
    if(!$stop -and $start){
        $indexDay = $indexDay.AddDays(1)
        $daycode = (get-date $indexDay -Format ddd)
        if($indexDay.Day -eq 1){
            $stop = $true
        }
    }
    if($daycode -eq "Sun"){
        $result += "`n"
    }
}
$result.Split("`n") | ?{$_.ToString().Trim().Length -gt 0} 
}

