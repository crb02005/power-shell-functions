import yaml
import json
 

import sys

filename = sys.argv[1:][0]
with open(filename, 'r') as myfile:
    yml = myfile.read()

data = yaml.load(yml)
json = json.dumps(data)
 
print(json)
