
<#
    PowerShell Python Functions
    Copyright (C) 2016  Carl Burks

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#>

function Get-RedditSubmissions($subreddit, $user, $version, $limit, $getType){
<#

.SYNOPSIS

Wraps the python reddit api wrapper for the shell

.EXAMPLE

Download from a subreddit

Get-RedditSubmissions "news" "myuser" "0.0.0.1" 25 "get_new" 


#>
    $scriptlocation = Resolve-Path($script:moduleRoot +"\functions\lswpraw.py")
    
    python.exe ($scriptlocation) ("--user",$user,"--subreddit",$subreddit,"--version",$version,"--getType",$getType,"--limit",$limit)
}

