import praw, argparse, json

parser = argparse.ArgumentParser(description='Limited Shell Wrapper around The Python Reddit Api Wrapper')
parser.add_argument("--user",help="Reddit User Name (used for user agent)")
#parser.add_argument("--password",help="Reddit Password Name")
parser.add_argument("--subreddit",help="Subreddit Name")
parser.add_argument("--version",help="Subreddit Name (used for subreddit name)")
parser.add_argument("--limit", default=25, help="Maximum number of records")
parser.add_argument("--getType", default="get_top", help="Get method", choices=["get_top","get_rising","get_new"])
args = parser.parse_args()

agent = "%s by /u/%s" % (args.version,args.user)

r = praw.Reddit(user_agent=agent)
submissions = getattr(r.get_subreddit(args.subreddit),args.getType)(limit=int(args.limit))
data = {}
data["linksubmissions"] = []
data["selfsubmissions"] = []

for submission in submissions:
    currentSubmission = {}
    currentSubmission["title"]= submission.title    
    currentSubmission["user"]= submission.author.name
    if not submission.is_self:
        currentSubmission["url"]= submission.url
        data["linksubmissions"].append(currentSubmission)
    else:
        currentSubmission["text"]= submission.selftext
        data["selfsubmissions"].append(currentSubmission)        

print json.dumps(data)
