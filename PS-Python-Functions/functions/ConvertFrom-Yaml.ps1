
<#
    PowerShell Python Functions
    Copyright (C) 2016  Carl Burks

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#>

function ConvertFrom-Yaml($path){
<#

.SYNOPSIS

Converts Yaml to JSON

.EXAMPLE

Convert Yaml file to JSON

ConvertFrom-Yaml $directoryPath 


#>
    $scriptlocation = Resolve-Path($script:moduleRoot +"\functions\yaml2.py")
    python.exe ($scriptlocation) (""+(resolve-path $path).Path+"")
}

