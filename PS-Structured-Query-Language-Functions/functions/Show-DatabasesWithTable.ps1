
<#
    PowerShell Get DatabaseWithTable Function
    Copyright (C) 2016  Carl Burks

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#>

function Show-DatabasesWithTable($server, $tableName){
<#

.SYNOPSIS

Shows databases with a specific table.

.EXAMPLE

Shows databases with a specific table.

Show-DatabasesWithTable "mycoolserver" "widgets"

#>
    $query = ""
    #if full permissions this works
    <#Get-Sql $server "master" "select name from sys.databases" | %{
        $database = $_.name
       if($query.Length -gt 0 ){
        $query += " union "
       }
       $query  += "select '$database' databaseName from $database.sys.tables where name = '$tableName'"
    }
    Get-Sql $server $database  | %{
       $database
    }
    return get-sql $server "master" $query %{$_.databaseName}#>
    # More calls but more robust
    Get-Sql $server "master" "select name from sys.databases" | %{
       $database = $_.name
       if((get-sql $server $database "select 1 from sys.tables where name = '$tableName'").length -gt 0){
        $database
       }
    }

}

