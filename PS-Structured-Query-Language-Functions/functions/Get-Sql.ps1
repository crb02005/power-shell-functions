
<#
    PowerShell Get Sql Function
    Copyright (C) 2016  Carl Burks

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#>

function Get-Sql($server, $database, $query){
<#

.SYNOPSIS

Gets sql from database.

.EXAMPLE

Gets sql from a database.

Get-Sql "mycoolserver" "mydb" "select * from widgets"

#>
    $connection = New-Object System.Data.SqlClient.SqlConnection 
    $connection.ConnectionString = "MultipleActiveResultSets=True;Integrated Security=True;Server="+$server+";DataBase="+$database
    $connection.open()
     
    $SqlCmd = New-Object System.Data.SqlClient.SqlCommand
    $SqlCmd.CommandText = $query
    $sqlCmd.Connection = $connection
    
    $SqlAdapter = New-Object System.Data.SqlClient.SqlDataAdapter
    $SqlAdapter.SelectCommand = $SqlCmd

    $DataSet = New-Object System.Data.DataSet
    $SqlAdapter.Fill($DataSet) > $null #Spams Count
    $connection.Close()

    return $DataSet.Tables[0]
}

