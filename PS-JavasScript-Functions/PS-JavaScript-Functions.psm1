param([parameter(Position=0, Mandatory = $false)][boolean]$debugMode = $false)
# Only functions with a dash are public
$script:moduleRoot = Split-Path -Path $MyInvocation.MyCommand.Path

# Dot source functions
"$script:moduleRoot\functions\*.ps1" | Resolve-Path | %{. $_.ProviderPath}

Export-ModuleMember -Function *
