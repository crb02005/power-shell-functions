
<#
    PowerShell JavaScript Functions
    Copyright (C) 2018  Carl Burks

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#>


function Write-JavascriptFlag(){
    Param($data, $prefix)
<#

.SYNOPSIS
Creates flags for use with JavaScript

.EXAMPLE

@" 
almost 
half 
ten 
quarter 
twenty 
five 
minutes 
to 
past 
"@ | Write-JavascriptFlag -prefix "clock_"


//Flags
const 
 flag_clock_almost  = 1,
 flag_clock_half  = 2,
 flag_clock_ten  = 4,
 flag_clock_quarter  = 8,
 flag_clock_twenty  = 16,
 flag_clock_five  = 32,
 flag_clock_minutes  = 64,
 flag_clock_to  = 128,
 flag_clock_past  = 256.

#>
$i = 1; 
$output = ""
$output += "`r`n//Flags" 
$output += "`r`nconst " 
@" 
almost 
half 
ten 
quarter 
twenty 
five 
minutes 
to 
past 
"@.Split("`n") | %{ 
 
$output += "`r`n flag_"+$prefix+$_+" = "+ [math]::Pow(2,$i-1)+"," 
$i +=1; 
}
return $output.trim(",")+";"
    }


