
<#
    PowerShell Humor Functions
    Copyright (C) 2018  Carl Burks

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#>

function ConvertTo-Mocking-Case(){
    Param($data)
<#

.SYNOPSIS
an
ConVerTS to a mOCkIng cApitiAlizAtION

.EXAMPLE

"converts to a mocking capitialization" | ConvertTo-Mocking-Case

"ConVerTS to a mOCkIng cApitiAlizAtION"

#>
if(($data -ne $null) -and ($data -ne '')){
    $str = $data
} else {
    $str = $input | Out-String
}
$str = $str.ToLower()
    return [Char[]]$str | %{
        if((Get-Random 10) -lt 7){Write-Host -NoNewline ([string]$_).ToUpper()} else {write-host -nonewline $_}
        }
    }


