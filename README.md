# PowerShell Functions

```
Copyright (C) 2016  Carl Burks

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
```


Simply place in your module directory and import.

[Installing a PowerShell Module](https://msdn.microsoft.com/en-us/library/dd878350%28v=vs.85%29.aspx?f=255&MSPPError=-2147217396)


### Importing

```
...
Import-Module PS-File-Functions -Force -Verbose
Import-Module PS-Calendar-Functions -Force -Verbose
Import-Module PS-Structured-Query-Language-Functions -Force -Verbose
Import-Module PS-GUI-Functions -Force -Verbose
...

```

### Usage
For list of functions:

```
Get-Command -Module PS-File-Functions
```

For help
```
Get-Help Write-CompressedFile -examples
```
