
<#
    PowerShell File Functions
    Copyright (C) 2016  Carl Burks

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#>

function ConvertTo-Base64{
Param($data)

<#

.SYNOPSIS

Converts data to Base64 string

.EXAMPLE

Convert data to Base64 string

ConvertTo-Base64 'SomeString'

.EXAMPLE

Convert data to Base64 string

cat myfile.txt | ConvertTo-Base64
#>
if(($data -ne $null) -and ($data -ne '')){
    $str = $data
} else {
    $str = $input | Out-String
}
if(($str -ne $null) -and ($str -ne '')){
    [System.Convert]::ToBase64String([System.Text.Encoding]::UTF8.GetBytes($str))
}
}
