﻿
<#
    PowerShell File Functions
    Copyright (C) 2018  Carl Burks

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#>

Add-Type -AssemblyName System.IO.Compression.FileSystem

function Write-UncompressedZipFile
{
<#

.SYNOPSIS

Uncompresses a zip file

.EXAMPLE

Uncompresses a zip file

Write-UncompressedZipFile $sourceZipFile $destination

#>
    param([string]$sourceZipFile, [string]$destination)

    [System.IO.Compression.ZipFile]::ExtractToDirectory($sourceZipFile, $destination)
}


