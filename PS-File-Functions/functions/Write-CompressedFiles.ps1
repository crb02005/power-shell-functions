
<#
    PowerShell File Functions
    Copyright (C) 2016  Carl Burks

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#>

Add-Type -AssemblyName "system.io.compression.filesystem"
function Write-CompressedFile($sourceDirectory, $destination, [boolean]$force = $false){
<#

.SYNOPSIS

Compress a directory

.EXAMPLE

Compress a directory to a file

Write-CompressedFile $directoryPath $destinationFile

.EXAMPLE

Compress a directory to a file and overwrite existing

Write-CompressedFile $directoryPath $destinationFile $True

#>
    $okayToContinue = $true
    if((Test-Path $destination)) {
        if($force){
            Remove-Item $destination
           
        }  else {
            $okayToContinue = $false
            Write-Error "$destination already exists."      
        } 
    } 
    if($okayToContinue){
        [System.IO.Compression.zipfile]::CreateFromDirectory($sourceDirectory, $destination)
    }
}

